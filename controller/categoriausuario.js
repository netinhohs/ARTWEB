var categoriausuario = {

    listar: function() {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/index.php/categoriausuario",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data)
                for (var i = 0; i < data.length; i++) {
                    $("#tableCategoriaUsuario").append("<tr>" +
                        "<td>" + data[i].cat_usu_id + "</td>" +
                        "<td>" + data[i].cat_usu_descricao + "</td>" +  
                        '<td>  <a href="manter_categoriausuario.php?cat_usu_id=' + data[i].cat_usu_id + '" class="btn btn-xs green" title="Alterar Categoria do usuário">'+
                            '<i class="fa fa-edit"></i>' +
                            '</a>'+
                            '<a href="javascript:;" onClick="Excluir(' + data[i].cat_usu_id + ')" class="btn btn-xs red" title="Deletar categoria do usuário">'+
                            '<i class="fa fa-trash"></i>' +
                            '</a>'+
                        '</td>'+                      
                        "</tr>"
                    );

                };
                carregarTabela();
            },
            error: function(data) {
                console.log(data);
            }
        });
    },
    listaById:function(id){
		$.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/categoriausuario/"+id,
         type:"GET",
         dataType:"JSON",
         success:function(data){
			 $("#cat_usu_descricao").val(data.cat_usu_descricao);
			 $("#cat_usu_id").val(data.cat_usu_id);
            console.log(data);
			
         },
         error:function(data){
            console.log(data);
         }
      });
	},   
	alterar:function(id){
		var serialize = $("#form1").serializeObject();
        console.log(JSON.stringify(serialize));
		$.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/categoriausuario/atualizar/"+id,
         type:"PUT",
         dataType:"JSON",
		 data: JSON.stringify(serialize),
         success:function(data){
            if (data.status == true){
               $("#txtSucesso").html("Alteração executada com sucesso!");
               $("#msgSucesso").show();
            }
            console.log(data);
         },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        
            $("#txtErro").html("Ocorreu algum erro na alteração, entrar em contato com o suporte!");
            $("#msgErro").show();
         }
      });
	},
	inserir:function(){
		var serialize = $("#form1").serializeObject();
		$.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/categoriausuario/inserir/",
         type:"POST",
         dataType:"JSON",
		 data: JSON.stringify(serialize),
         success:function(data){
            if (data.id > 0){
               $("#txtSucesso").html("Incluido com sucesso!");
               $("#msgSucesso").show();
               $("#cat_usu_descricao").val();
            }
            console.log(data);
         },
         error:function(data){
            $("#txtErro").html("Ocorreu algum erro na inclusão, entrar em contato com o suporte!");
            $("#msgErro").show();
            console.log(data);
         }
      });
	},
   excluir:function(id){
      $.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/categoriausuario/delete/"+id,
         type:"DELETE",
         dataType:"JSON",
         success:function(data){
            if (data.status == true){
               bootbox.alert("Item excluido com sucesso!", function() {
                   window.location.reload();
               });
            }
            console.log(data);
         },
         error:function(data){
           bootbox.alert(data);
            console.log(data);
         }
      });
   },
}
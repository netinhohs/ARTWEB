var obra = {
    listar: function() {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/obra",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data)
                for (var i = 0; i < data.length; i++) {
                    $("#addcoluna").append('<tr>' +
                        '<td> ' + data[i].obr_id + '</td>' +
                        '<td> ' + data[i].obr_descricao + '</td>' +
                        '<td> ' + data[i].cat_obra_descricao + ' </td>' +
                        '<td style="text-align: center;"> <a href="detalhar_obra.php?obr_id=' + data[i].obr_id + '" class="btn btn-xs green" title="Detalhar obra">' +
                        '<i class="fa fa-eye"></i>' +
                        '</a>' +
                        '</td>' +
                        '</tr>');
                }
                carregarTabela();
            },
            error: function(data) {

            }
        });
    },
    listaObraArtistaByID: function(id) {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/obraartista/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data)
                for (var i = 0; i < data.length; i++) {
                    $("#addcoluna").append('<tr>' +
                        '<td style="text-align: center;"> <a title="Detalhar obra" href="detalhar_obra.php?obr_id=' + data[i].obr_id + '">' + data[i].obr_id + '</a></td>' +
                        '<td> ' + data[i].obr_descricao + '</td>' +
                        '<td> ' + data[i].cat_obra_descricao + ' </td>' +
                        '</tr>');
                }
                carregarTabela();
            },
            error: function(data) {
                console.log(data);
            }
        });
    },
    listaObraByID: function(id) {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/obra/imagem/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) { 
                console.log(data);
                $("#obr_id").val(data[0].obr_id);
                $("#obr_descricao").val(data[0].obr_descricao);
                $("#cat_obra_descricao").val(data[0].cat_obra_descricao);
                $("#fotoobra").html("<img src='https://www.doocati.com.br/tcc/client/" + data[0].img_url + "' style='max-width: 200px;' />");
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
}
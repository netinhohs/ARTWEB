var artistasbemavaliados = {

    listar: function() {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/usuario/artistasbemavaliados/",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data)
                for (var i = 0; i < data.length; i++) {
                    $("#tableRankingArtistas").append("<tr>" +
                        "<td>" + data[i].usu_id + "</td>" +
                        "<td>" + data[i].usu_nome + "</td>" +
                        "<td>" + data[i].usu_email + "</td>" +
                        "<td>" + data[i].ava_titulo + "</td>" +
                        "<td>" + data[i].ava_descricao + "</td>" +
                        "<td>" + data[i].media + "</td>" +
                        "</tr>"
                    );

                };
                carregarTabela();
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
}
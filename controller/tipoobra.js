var tipoobra = {

   listar:function(){
      $.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/tipoobra",
         type:"GET",
         dataType:"JSON",
         success:function(data){
            console.log(data)
            for(var i = 0; i < data.length; i++){
               $("#addcoluna").append('<tr>'+
                     '<td> ' + data[i].cat_obra_id + '</td>'+
                     '<td> ' + data[i].cat_obra_descricao + '</td>'+
                     '<td>  <a href="manter_categoria.php?cat_obra_id=' + data[i].cat_obra_id + '" class="btn btn-xs green" title="Alterar Categoria">'+
                            '<i class="fa fa-edit"></i>' +
                            '</a>'+
                            '<a href="javascript:;" onClick="Excluir(' + data[i].cat_obra_id + ')" class="btn btn-xs red" title="Deletar Categoria">'+
                            '<i class="fa fa-trash"></i>' +
                            '</a>'+
                      '</td>'+
                  '</tr>');
            }
            carregarTabela();
         },
         error:function(data){
            
         }
      });   
	},
	listaById:function(id){
		$.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/tipoobra/"+id,
         type:"GET",
         dataType:"JSON",
         success:function(data){
			 $("#cat_obra_descricao").val(data.cat_obra_descricao);
			 $("#cat_obra_id").val(data.cat_obra_id);
            console.log(data);
			
         },
         error:function(data){
            console.log(data);
         }
      });
	},   
	alterar:function(id){
		var serialize = $("#form1").serializeObject();
		$.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/tipoobra/"+id,
         type:"PUT",
         dataType:"JSON",
		 data: JSON.stringify(serialize),
         success:function(data){
            if (data.status == true){
               $("#txtSucesso").html("Alteração executada com sucesso!");
               $("#msgSucesso").show();
            }
            console.log(data);
         },
         error:function(data){
            $("#txtErro").html("Está categoria já existe. Por favor informe outra!");
            $("#msgErro").show();
         }
      });
	},
	inserir:function(){
		var serialize = $("#form1").serializeObject();
		$.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/tipoobra/",
         type:"POST",
         dataType:"JSON",
		 data: JSON.stringify(serialize),
         success:function(data){
            if (data.id > 0){
               $("#txtSucesso").html("Incluido com sucesso!");
               $("#msgSucesso").show();
            }
            console.log(data);
         },
         error:function(data){
            $("#txtErro").html("Ocorreu algum erro na inclusão, entrar em contato com o suporte!");
            $("#msgErro").show();
            console.log(data);
         }
      });
	},
   excluir:function(id){
      $.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/tipoobra/"+id,
         type:"DELETE",
         dataType:"JSON",
         success:function(data){
            if (data.status == true){
               bootbox.alert("Item excluido com sucesso!", function() {
                   window.location.reload();
               });
            }
            console.log(data);
         },
         error:function(data){
           bootbox.alert(data);
            console.log(data);
         }
      });
   },   
   
}
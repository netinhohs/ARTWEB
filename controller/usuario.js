var usuario = {

   login:function(email, senha){
      $.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/login/"+email+"/"+senha,
         type:"GET",
         dataType:"JSON",
         success:function(data){
            console.log(data.usu_id);

            if(data === false){
               $("#txtErro").html("Usuário não encontrado!");
               $("#msgErro").show();
            }else{
               $.cookie("logado", '1');
               if($("input[name='remember']").is(':checked')){
                  $.cookie("usu_email",data.usu_email);
                  $.cookie("usu_senha",data.usu_senha);
               }
               window.location.href = 'dashboard.php';
            }
         },
         error:function(data){
            console.log(data);
         }
      });   
	}
}
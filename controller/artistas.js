var artistas = {

    listarArtistas: function() {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/artistasPedentes/",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var cpf = "";
                    if (data[i].usu_cpf == null) {
                        cpf = "Ainda está para ser informado no cadastro completo";
                    } else {
                        cpf = data[i].usu_cpf;;;
                    }
                    $("#tableArtistas").append("<tr>" +
                        "<td>" + data[i].usu_id + "</td>" +
                        "<td>" + data[i].usu_nome + "</td>" +
                        "<td>" + data[i].usu_email + "</td>" +
                        "<td>" + cpf + "</td>" +
                        "<td>" + data[i].cat_usu_descricao + "</td>" +
                        '<td> <a href="javascript:;" onClick="Excluir(' + data[i].usu_id + ')" class="btn btn-xs blue" title="Click para ativar o artista" >' +
                        '<i class="fa fa-check"></i>' +
                        '</a>' +
                        '</td>' +
                        "</tr>");
                };
                carregarTabela();
            },
            error: function(data) {
                console.log(data);
            }
        });
    },
    ativar: function(id) {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/ativarArtistasPedentes/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                if (data.status == true) {
                    bootbox.alert("Artista ativado com sucesso!", function() {
                        window.location.reload();
                    });
                }
                console.log(data);
            },
            error: function(data) {
                bootbox.alert(data);
                console.log(data);
            }
        });
    },
    desativar: function(id) {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/desativarArtistasPedentes/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                if (data.status == true) {
                    bootbox.alert("Artista desativado com sucesso!", function() {
                        window.location.reload();
                    });
                }
                console.log(data);
            },
            error: function(data) {
                bootbox.alert(data);
                console.log(data);
            }
        });
    },

    listarTodos: function() {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/artistas/",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    if (data[i].usu_cpf == null) {
                        cpf = "Ainda está para ser informado no cadastro completo";
                    } else {
                        cpf = data[i].usu_cpf;;;
                    }
                    $("#tableArtistas").append("<tr>" +
                        "<td>" + data[i].usu_id + "</td>" +
                        "<td>" + data[i].usu_nome + "</td>" +
                        "<td>" + data[i].usu_email + "</td>" +
                        "<td>" + cpf + "</td>" +
                        "<td>" + data[i].cat_usu_descricao + "</td>" +
                        '<td style="text-align: center;"> <a href="detalhar_artista.php?usu_id=' + data[i].usu_id + '" class="btn btn-xs green" title="Detalhar Artista">' +
                        '<i class="fa fa-eye"></i>' +
                        '</a>' +
                        '</td>' +
                        "</tr>");
                };
                carregarTabela();
            },
            error: function(data) {
                console.log(data);
            }
        });
    },
    listarByID: function(id) {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/usuario/detalhar/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                $("#usu_id").val(data.usu_id);
                $("#usu_email").val(data.usu_email);
                $("#usu_cpf").val(data.usu_cpf);
                $("#usu_genero").val(data.usu_genero);
                $("#usu_nome").val(data.usu_nome);
                $("#usu_data_nascimento").val(data.usu_data_nascimento);
                $("#usu_ativo").val(data.usu_ativo);
                $("#cat_usu_id").val(data.cat_usu_id);
                $("#usu_cadastro_completo").val(data.usu_cadastro_completo);
                $("#usu_imagem").val(data.usu_imagem);
                $("#usu_telefone").val(data.usu_telefone);
                $("#usu_celular").val(data.usu_celular);
                if (data.usu_imagem != "") {
                    if (data.usu_imagem.substr(0, 4) == "http") {
                        $("#foto").html("<img src='" + data.usu_imagem + "' style='max-width: 200px;' />");
                    } else {
                        $("#foto").html("<img src='https://www.doocati.com.br/tcc/client/" + data.usu_imagem + "' style='max-width: 200px;' />");
                    }
                } else {
                    $("#fotoobra").html("<img src='https://www.doocati.com.br/tcc/client/images/user.jpg' style='max-width: 200px;' />");
                }

                if (data.usu_ativo == "0") {
                    $("#btnSalvar").attr("disabled", true);
                }

                console.log(data);
            },
            error: function(data) {
                console.log(data);
            }
        });
    },
    listarArtistaObra: function(id) {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/usuario/listarartistadaobra/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);

                $("#addcolumn").append('<tr>' +
                    '<td style="text-align: center;"> <a title="Detalhar artista" href="detalhar_artista.php?usu_id=' + data.usu_id + '">' + data.usu_id + '</a></td>' +
                    '<td>' + data.usu_nome + '</td>' +
                    '<td>' + data.usu_email + '</td>' +
                    '</tr>');

                //carregarTabela();
                //console.log(data);
            },
            error: function(data) {
                console.log(data);
            }
        });
    },

    listarAtivos: function() {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/usuario/artistasAtivos",
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var cpf = "";
                    if (data[i].usu_cpf == null) {
                        cpf = "Ainda está para ser informado no cadastro completo";
                    } else {
                        cpf = data[i].usu_cpf;;;
                    }
                    $("#tableAtivos").append("<tr>" +
                        "<td>" + data[i].usu_id + "</td>" +
                        "<td>" + data[i].usu_nome + "</td>" +
                        "<td>" + data[i].usu_email + "</td>" +
                        "<td>" + cpf + "</td>" +
                        "<td>" + data[i].cat_usu_descricao + "</td>" +
                        "</tr>");
                };
                carregarTabela();
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
}
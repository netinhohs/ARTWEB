
<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
        <li class="nav-item start <?php if(empty($menu)){ ?>active open <?php } ?>">
            <a href="dashboard.php" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
                <span class="selected"></span>
                <span class="arrow open"></span>
            </a>
        </li>
        <li class="nav-item <?php if($menu == 'artista'){ ?>active open <?php } ?> ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-users"></i>
                <span class="title">Artistas</span>
                <span class="arrow"></span>
            </a>
             <ul class="sub-menu">
                <li class="nav-item  <?php if($menu == 'artista'){ ?>active open <?php } ?> ">
                    <a href="listar_todos.php" class="nav-link ">
                        <span class="title">Todos</span>
                    </a>
                </li>
                <li class="nav-item  <?php if($menu == 'artista'){ ?>active open <?php } ?> ">
                    <a href="listar_artistas_avaliados.php" class="nav-link ">
                        <span class="title">Bem avaliados</span>
                    </a>
                <li class="nav-item  <?php if($menu == 'artista'){ ?>active open <?php } ?> ">
                    <a href="listar_artistas.php" class="nav-link ">
                        <span class="title">Pendentes</span>
                    </a>
                </li>
                <li class="nav-item  <?php if($menu == 'artista'){ ?>active open <?php } ?> ">
                    <a href="listar_ativos.php" class="nav-link ">
                        <span class="title">Ativos</span>
                    </a>
                </li>
                </li>
                </ul>
                
        </li>
        <li class="nav-item <?php if($menu == 'categoria'){ ?>active open <?php } ?> ">
            <a href="listar_categorias.php" class="nav-link nav-toggle">
                <i class="icon-tag"></i>
                <span class="title">Categorias</span>
                <span class="arrow"></span>
            </a>
        </li>
        <li class="nav-item <?php if($menu == 'categoriausuario'){ ?>active open <?php } ?> ">
            <a href="listar_categoriausuario.php" class="nav-link nav-toggle">
                <i class="icon-direction"></i>
                <span class="title">Categorias Usuários</span>
                <span class="arrow"></span>
            </a>
        </li>
        <li class="nav-item <?php if($menu == 'obra'){ ?>active open <?php } ?> ">
            <a href="listar_obras.php" class="nav-link nav-toggle">
                <i class="icon-globe"></i>
                <span class="title">Obras</span>
                <span class="arrow"></span>
            </a>
        </li>
        <!--<li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-bulb"></i>
                <span class="title">Elements</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="elements_steps.html" class="nav-link ">
                        <span class="title">Steps</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="elements_lists.html" class="nav-link ">
                        <span class="title">Lists</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="elements_ribbons.html" class="nav-link ">
                        <span class="title">Ribbons</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="elements_overlay.html" class="nav-link ">
                        <span class="title">Overlays</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="elements_cards.html" class="nav-link ">
                        <span class="title">User Cards</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item  ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-briefcase"></i>
                <span class="title">Tables</span>
                <span class="arrow"></span>
            </a>
        </li>-->
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Admin | Artesanato</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/pages/css/login-4.min.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" /> 
    </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <!-- <a href="index.html"> -->
             <!--  img aqui se quiser -->
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" id="form1">
                <h3 class="form-title">Admin | Artesanato</h3>
                <div id="msgErro" class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span id="txtErro"> Campos obrigatórios. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="email" required placeholder="E-mail" id="email" name="email" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Senha</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" required autocomplete="off" placeholder="Senha" id="senha" name="senha" /> </div>
                </div>
                <div class="form-actions">
                    <label class="checkbox">
                        <input type="checkbox" name="remember" value="1" /> Lembrar </label>
                    <button type="button" id="btnLogin" class="btn green pull-right"> Login </button>
                </div>
                <div class="forget-password">
                    <h4>Esqueceu a senha ?</h4>
                    <p> Click
                        <a href="javascript:;" id="forget-password"> AQUI </a> </p>
                </div>
            </form>
            <form class="forget-form" action="index.php" method="post">
                <p> Digite seu e-mail para recuperar a senha. </p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn red btn-outline"> Voltar </button>
                    <button type="button" id="btnEsqueci" class="btn green pull-right"> Enviar </button>
                </div>
            </form>
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> Admin | Artesanato </div>
        <!-- BEGIN CORE PLUGINS -->
        <?php include 'include/scripts.php'; ?>
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/localization/messages_pt_BR.js"></script>
        <script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="assets/pages/scripts/login-4.min.js" type="text/javascript"></script>
    </body>
    <script src="controller/usuario.js"></script>
    <script type="text/javascript">

    jQuery(document).ready(function() {
        $("#form1").validate();

        if($.cookie('usu_email') != "" && $.cookie('usu_senha')){
            usuario.login($.cookie('usu_email'), $.cookie('usu_senha'));
        }
    });   

    $("#btnLogin").on("click", function(){
        if($("#form1").valid()){ // se os campos estiverem validados (preenchidos) retorna true
            var email = $("#email").val();
            var senha = $("#senha").val();

            usuario.login(email, senha);
        }
    });

    <?php if(!empty($_GET['timeout'])){ ?>
        $("#txtErro").html("Você precisa estar logado para executar essa ação.");
        $("#msgErro").show();
    <?php } ?>

    </script>

</html>
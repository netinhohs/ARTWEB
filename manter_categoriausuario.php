<!DOCTYPE html>
<html lang="en">
    
    <?php include "include/head.php"; ?>    

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        
        <?php include 'include/header.php'; ?>

        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <?php $menu = 'categoriausuario'; ?>
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                <?php include 'include/menu.php'; ?>
            </div>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Categoria de usuário
                        <small>Manuten&ccedil;&atilde;o de categoria dos usuários</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="icon-user"></i>
                                <a href="listar_categoriausuario.php">Categorias de usuário</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Manuten&ccedil;&atilde;o</a>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                        <?php include "include/messagens.php" ?>
                            <div class="portlet light">
                                <div class="portlet-body form">
                                    <form class="form-horizontal" id="form1" role="form">
                                        <div class="form-body">
                                            <div id="msgErro" class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                <span id="txtErro"> Campos obrigatórios. </span>
                                            </div>
											<input type="hidden" name="cat_usu_id" id="cat_usu_id">

                                            <div class="form-group">
                                                <label class="control-label col-md-2">Nome
                                                    <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-10">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" required id="cat_usu_descricao" class="form-control"  name="cat_usu_descricao" />
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <div class="col-md-12 " >
                                                   <button type="button" style="float:right;" id="btnSalvar" class="btn default btn">Salvar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Albérico Carvalho.
                <a href="#" title="Entre em contato" target="_blank">albericoneto.carvalho@gmail.com</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <?php include 'include/scripts.php'; ?>
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/localization/messages_pt_BR.js"></script>
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
    </body>
	<script src="controller/categoriausuario.js"></script>
    <script src="controller/verifica.js"></script>
    <script type="text/javascript">
    document.title ="Categorias";
        jQuery(document).ready(function() {
            $("#form1").validate();
        });   

		<?php if(isset($_GET["cat_usu_id"])){ ?>
			categoriausuario.listaById(<?php echo $_GET["cat_usu_id"]; ?>);
		<?php } ?>
		
		$("#btnSalvar").on("click",function(){
			
            if($("#form1").valid()){
                if($("#cat_usu_id").val() != ""){
                    categoriausuario.alterar($("#cat_usu_id").val());
                }
                else{
                    categoriausuario.inserir(); 
                }
            }
		});
    </script>

</html>
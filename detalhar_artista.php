<!DOCTYPE html>
<html lang="en">
    
    <?php include "include/head.php"; ?>    

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        
        <?php include 'include/header.php'; ?>

        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <?php $menu = 'artista'; ?>
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                <?php include 'include/menu.php'; ?>
            </div>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Artista
                        <small>Detalhes do Artista</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="icon-tag"></i>
                                <a href="listar_todos.php">Artistas</a>
                                <i class="fa fa-users"></i>
                            </li>
                            <li>
                                <a href="#">Detalhe</a>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                        <?php include "include/messagens.php" ?>
                            <div class="portlet light">
                                <div class="portlet-body form">
                                    <form class="form-horizontal" id="form1" role="form">
                                        <div class="form-body">
                                            <div id="msgErro" class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                <span id="txtErro"> Campos obrigatórios. </span>
                                            </div>
											<input type="hidden" name="usu_id" id="usu_id">

                                            <!-- inicio de campos -->
                                            <!-- campo nome -->
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Nome: 
                                                    <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-10">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" required id="usu_nome" class="form-control" name="usu_nome" readonly/>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- campo email -->
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Email:
                                                    <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-10">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" required id="usu_email" class="form-control" name="usu_email" readonly/>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- campo cpf -->
                                            <div class="form-group">
                                                <label class="control-label col-md-2">CPF:
                                                    <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-10">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" required id="usu_cpf" class="form-control" name="usu_cpf" readonly/>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- campo genero -->
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Genero:
                                                    <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-10">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" required id="usu_genero" class="form-control" name="usu_genero" readonly/>
                                                    </div>
                                                </div>
                                            </div>

											<div class="form-group">
                                                <label class="control-label col-md-2">Imagem</label>
                                                <div class="col-md-2">
                                                    <div id="foto"></div>
                                                </div>
                                                <div class="col-md-8 " >
                                                   <button type="button" style="float:right;" id="btnSalvar" onclick="Desativar()" class="btn default btn">Desativar Artista</button>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </form>

                                    <!-- Area de listagem das obras do artista e dos atelies-->
                                    
                                    <div class="row">
                                        <!-- inicio tabela de listagem das obras-->
                                        <div class="col-md-6">
                                        <div class="portlet">
                                            <div class="portlet-title">
                                                <div class="caption">Lista de obras </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-bordered table-advance table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th> ID </th>
                                                                <th class="hidden-xs"> Descrição </th>
                                                                <th> Categoria </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="addcoluna">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- fim listagem obras -->
                                    </div>
                                    <!-- inicio tabela de listagem dos atelies-->
                                        <div class="col-md-6">
                                        <div class="portlet">
                                            <div class="portlet-title">
                                                <div class="caption">Lista de Ateliês </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-bordered table-advance table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th> Nome </th>
                                                                <th> Endereço </th>
                                                                <th class="hidden-xs"> UF </th>                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody id="addcoluna2">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- fim listagem atelies-->
                                    </div>

                                    <!-- fim da area de listagem -->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Albérico Carvalho.
                <a href="#" title="Entre em contato" target="_blank">albericoneto.carvalho@gmail.com</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <?php include 'include/scripts.php'; ?>
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
    </body>
	<script src="controller/artistas.js"></script>
    <script src="controller/atelie.js"></script>
    <script src="controller/obra.js"></script>
    <script src="controller/verifica.js"></script>
    <script type="text/javascript">
         document.title ="Artista";
        function Desativar() {
             bootbox.confirm({ 
                message: "Tem certeza que deseja desativar o artista?", 
                callback: function(result){
                    if(result == true){
                         artistas.desativar(<?php echo $_GET["usu_id"]; ?>);
                    }
                }
            });           
        }
        jQuery(document).ready(function() {
            $("#form1").validate();
        });   
		<?php if(isset($_GET["usu_id"])){ ?>
			artistas.listarByID(<?php echo $_GET["usu_id"]; ?>);
            obra.listaObraArtistaByID(<?php echo $_GET["usu_id"]; ?>);
            atelie.listarAtelieArtistas(<?php echo $_GET["usu_id"]; ?>);
		<?php } ?>
    </script>

</html>
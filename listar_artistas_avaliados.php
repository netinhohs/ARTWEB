<!DOCTYPE html>
<html lang="en">
    
    <?php include "include/head.php"; ?>    
    <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        
        <?php include 'include/header.php'; ?>

        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <?php $menu = 'artista'; ?>
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                <?php include 'include/menu.php'; ?>
            </div>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Ranking de artistas bem avaliados
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.php">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="icon-basket"></i>
                                <a href="listar_pedidos.php">Categorias</a>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="listarrankingartistas">
                                        <thead>
                                            <tr>
                                                <th> ID </th>
                                                <th> Nome </th>
                                                <th> Email</th>
                                                <th>Título</th>
                                                <th>Descrição</th>
                                                <th>Média das notas</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tableRankingArtistas">
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner">
                <a href="#" title="Entre em contato" target="_blank"></a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <?php include 'include/scripts.php'; ?>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    </body>

    <script src="controller/rankingartistas.js"></script>
    <script>
        jQuery(document).ready(function() {
           artistasbemavaliados.listar();
        });
    </script>
    <script type="text/javascript">
        document.title ="Ranking de artistas bem avaliados";
        function carregarTabela(){
            var TableDatatablesColreorder = function () {

                var initTable1 = function () {
                    var table = $('#listarrankingartistas');

                    var oTable = table.dataTable({
                        
                        "language": {
                            "aria": {
                                "sortAscending": ": activate to sort column ascending",
                                "sortDescending": ": activate to sort column descending"
                            },
                            "emptyTable": "Nenhum resultado encontrado",
                            "info": "Mostrando _START_ a _END_ de _TOTAL_ linhas",
                            "infoEmpty": "nenhuma linha foi encontrada",
                            "infoFiltered": "(filtrado de _MAX_ linhas)",
                            "lengthMenu": "_MENU_ linhas",
                            "search": "Procurar: ",
                            "zeroRecords": "Nenhum registro encontrado"
                        },

                        buttons: [
                            { extend: 'print', className: 'btn dark btn-outline', text: 'Imprimir' },
                            { extend: 'pdf', className: 'btn green btn-outline', text: 'PDF' },
                            { extend: 'excel', className: 'btn purple btn-outline ', text: 'Excel' }
                        ],

                        responsive: true,

                        colReorder: {
                            reorderCallback: function () {
                                console.log( 'callback' );
                            }
                        },

                        "order": [
                            [0, 'asc']
                        ],
                        
                        "lengthMenu": [
                            [5, 10, 15, 20, -1],
                            [5, 10, 15, 20, "All"] // change per page values here
                        ],
                        // set the initial value
                        "pageLength": 10,

                        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
                    });
                }
                return {
                    init: function () {

                        if (!jQuery().dataTable) {
                            return;
                        }

                        initTable1();
                    }
                };
            }();
            TableDatatablesColreorder.init();
        }
    </script>

</html>
<!DOCTYPE html>
<html lang="en">
    
    <?php include "include/head.php"; ?>    

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        
        <?php include 'include/header.php'; ?>

        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                <?php include 'include/menu.php'; ?>
            </div>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Dashboard
                        
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.php">Dashboard</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="portlet light ">
                                
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bubble font-dark hide"></i>
                                        <span class="caption-subject font-hide bold uppercase">Gráficos</span>
                                    </div>
                                </div> 
                                
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>Graficos</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="width: 50%;float: left;">
                                            <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                                        </div>
                                        <div class="col-md-12" style="width: 50%;float: left;">
                                            <div id="chartContainer2" style="height: 300px; width: 100%;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2016 &copy; Albérico Carvalho.
                <a href="#" title="Entre em contato" target="_blank">albericoneto.carvalho@gmail.com</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <?php include 'include/scripts.php'; ?>
        <script src="controller/verifica.js"></script>
        <script src="controller/tipoobra.js"></script>
        <script type="text/javascript">
            document.title ="Dashboard";
             window.onload = function () {
                $.ajax({
                 url:"https://www.doocati.com.br/tcc/webservice/tipoobras/",
                 type:"GET",
                 dataType:"JSON",
                 success:function(data){         
                   //console.log(data);
                    var dados = [];
                    for (var i = 0; i < data.length; i++) {
                        dados.push({label: data[i].label, y: Number(data[i].y)});
                    }
                    console.log(dados);
                    var chart = new CanvasJS.Chart("chartContainer", {

                      title:{
                        text: "Quantidade de Obras por Tipo"              
                      },
                      data: [//array of dataSeries              
                        { //dataSeries object

                         /*** Change type "column" to "bar", "area", "line" or "pie"***/
                         type: "pie",
                         dataPoints: dados
                       }
                       ]
                     });

                    chart.render();
                 
                 },
                 error:function(data){
                    console.log(data);
                 }
              });
                $.ajax({
                 url:"https://www.doocati.com.br/tcc/webservice/avaliacoesgrupo/",
                 type:"GET",
                 dataType:"JSON",
                 success:function(data){         
                   //console.log(data);
                    var dados = [];
                    for (var i = 0; i < data.length; i++) {
                        dados.push({label: data[i].ava_nota, y: Number(data[i].y)});
                    }
                    console.log(dados);
                    var chart = new CanvasJS.Chart("chartContainer2", {

                      title:{
                        text: "Quantidade de Avaliações por Tipo"              
                      },
                      data: [//array of dataSeries              
                        { //dataSeries object

                         /*** Change type "column" to "bar", "area", "line" or "pie"***/
                         type: "column",
                         dataPoints: dados
                       }
                       ]
                     });

                    chart.render();
                 
                 },
                 error:function(data){
                    console.log(data);
                 }
              });

          }
        </script>
    </body>

</html>
<!DOCTYPE html>
<html lang="en">
    
    <?php include "include/head.php"; ?>    

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        
        <?php include 'include/header.php'; ?>

        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <?php $menu = 'obra'; ?>
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                <?php include 'include/menu.php'; ?>
            </div>
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title"> Obra
                        <small>Detalhes da Obra</small>
                    </h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="icon-tag"></i>
                                <a href="listar_todos.php">Obras</a>
                                <i class="fa fa-users"></i>
                            </li>
                            <li>
                                <a href="#">Detalhe</a>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                        <?php include "include/messagens.php" ?>
                            <div class="portlet light">
                                <div class="portlet-body form">
                                    <form class="form-horizontal" id="form1" role="form">
                                        <div class="form-body">
                                            <div id="msgErro" class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button>
                                                <span id="txtErro"> Campos obrigatórios. </span>
                                            </div>

                                            <!-- inicio de campos -->
                                            <!-- campo id -->
                                            <div class="form-group">
                                                <label class="control-label col-md-2">ID: 
                                                    <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-10">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" required id="obr_id" class="form-control" name="obr_id" readonly/>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- campo descrição -->
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Descrição:
                                                    <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-10">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" required id="obr_descricao" class="form-control" name="obr_descricao" readonly/>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- campo categoria -->
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Categoria:
                                                    <span class="required" aria-required="true"> * </span>
                                                </label>
                                                <div class="col-md-10">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" required id="cat_obra_descricao" class="form-control" name="cat_obra_descricao" readonly/>
                                                    </div>
                                                </div>
                                            </div>
                                              <div class="form-group">
                                                <label class="control-label col-md-2">Imagem:
                                                </label>
                                                <div class="col-md-10">
                                                    <div id="fotoobra"></div>
                                                       
                                                </div>
                                            </div>                                            
                                        </div>
                                        <!-- campo de alteraçao da Obra -->
                                        <div class="form-group">
                                                <div class="col-md-12 " >
                                                  
                                                </div>
                                            </div>
                                    </form>

                                    <!-- Area de listagem do artista da obra -->
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="portlet light ">
                                                <div class="portlet-title">
                                                <div class="caption">Artista</div>
                                                    <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th> ID </th>
                                                                <th> Nome </th>
                                                                <th> Email </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="addcolumn">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner">
                <a href="#" title="Entre em contato" target="_blank"></a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <?php include 'include/scripts.php'; ?>
        <script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
    </body>
	<script src="controller/artistas.js"></script>
    <script src="controller/obra.js"></script>
    <script src="controller/verifica.js"></script>
    <script type="text/javascript">
        document.title ="Obra";
        jQuery(document).ready(function() {
            $("#form1").validate();
        });   
		<?php if(isset($_GET["obr_id"])){ ?>
			obra.listaObraByID(<?php echo $_GET["obr_id"]; ?>);
            artistas.listarArtistaObra(<?php echo $_GET["obr_id"]; ?>);
		<?php } ?>
       
            
        
        




    </script>
</html>